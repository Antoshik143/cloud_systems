##### University: [ITMO University](https://itmo.ru/ru/)
##### Faculty: [FICT](https://fict.itmo.ru)
##### Course: Cloud computing and services
Year: 2023/2024

Group: K34202

Lab: Lab2-star

## Отчёт по лабораторной работе №2* "Создание сертификата для сервиса в Kubernetes"

### Цель работы
Настроить подключение к сервису в миникубе через https, используя самоподписанный сертификат.

### Ход работы

#### 1. Подготовка инфраструктуры 

Для начала мы запустили наш кластер minikube с драйвером docker, используя команду:

```
minikube start --driver=docker
```

![Minikube_start](./pictures/minikube.png)

Также в наших манифестах будет использоваться ingress, поэтому мы отдельно активировали данное дополнение, которое позволило скачать ingress контроллер в minikube:

```
minikube addons enable ingress
```

![addons_list](./pictures/addons_list.png)

Для настройки доменного имени мы создали запись в файле /etc/hosts, получив IP-адрес кластера minikube - 192.168.49.2 и добавив к нему имя - lab2-star.test.

![DNS](./pictures/dns.png)

#### 2. Создание манифестов

Для разворачивания инфраструктуры мы использовали 3 составляющих:
- Deployment, в котором описана спецификация нашего pod'а
- Service, которые позволяет связываться с pod'ом
- Ingress-controller на базе Nginx для обращения к серверу внутри pod'а

В качестве сервера мы выбрали vs code server, образ которого мы использовали внутри конифгов.

Конфигурация [app-deployment.yml](./app-deployment.yml), которая разворачивает внутри пода 1 контейнер на базе образа gitpod/openvscode-server и работает на порту 3000:

```
apiVersion: apps/v1
kind: Deployment

metadata:
  name: openvscode-server-deployment

spec:
  replicas: 1
  selector:
    matchLabels:
      components: lab2-star
  template:
    metadata:
      labels:
        components: lab2-star
    spec:
      containers:
        - name: openvscode-server
          image: gitpod/openvscode-server
          imagePullPolicy: Always
          ports:
            - containerPort: 3000
          resources:
            limits:
              memory: "128Mi"
              cpu: "150m"
```

Конфигурация [app-service.yml](./app-service.yml):

```
apiVersion: v1
kind: Service

metadata:
  name: openvscode-server-clusterip
  
spec:
  type: ClusterIP
  ports:
    - port: 3000
      protocol: TCP
  selector:
    components: lab2-star
```

И прежде чем создавать ingress.yml мы создали самоподписанный сертификат.

#### 3. Создание самоподписанного сертификата

Для создания сертификата мы использовали утилиту openssl со следующими параметрами:

- x509 — уточнение, что нам нужен именно самоподписанный сертификат;
- newkey — автоматическое создание ключа сертификата;
- days — срок действия сертификата в днях;
- keyout — путь и имя файла ключа;
- out —  путь и имя файла сертификата.

```
openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout privateKey.key -out certificate.crt
```

![create_certificate](./pictures/create_certificate.png)

В результате мы имеем 2 новых файла: сам сертификат и файл ключа.

![Keys](./pictures/keys.png)

Дальше при помощи сертификата мы создали секрет Kubernetes

![create_secret](./pictures/create_secret.png)

И в итоге написали [ingress.yml](./ingress.yml), в который подложили ранее созданный секрет.

```
apiVersion: networking.k8s.io/v1
kind: Ingress

metadata:
  name: myingress
  annotations:
    nginx.ingress.kubernetes.io/add-base-url: "true"

spec:
  ingressClassName: nginx
  tls:
    - hosts:
      - lab2-star.test
      secretName: lab2-tls
      
  rules:
  - host: lab2-star.test
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: openvscode-server-clusterip
            port: 
              number: 3000
```

#### 4. Создание кластера

Для запуска всей инфраструктуры использовались следующие команды:
```
kubectl apply -f app-deployment.yml
kubectl apply -f app-service.yml
kubectl apply -f ingress.yml
```

В результате имеем:

![kubectl_all](./pictures/infrustructure.png)

В адресной строке браузера ввели наше настроенное доменное имя lab2-star.test и получили предупреждение от браузера о непроверенном сертификате.

![Warning](./pictures/warning.png)

Мы приняли данное прдупреждение с сертификатом и попали на страницу нашего веб сервера, используя самоподписанный сертификат.

![Server_vs_code](./pictures/server_vs_code.png)


### Вывод:

В результате выполнения лабораторной работы мы настроили подключение к сервису в миникубе через https, используя самоподписанный сертификат.



