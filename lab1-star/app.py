import os
import psycopg2

from flask import (
    Flask,
    Response,
)


app = Flask(__name__)


def requests() -> None:
    with psycopg2.connect(
            database=os.getenv("POSTGRES_DB"),
            user=os.getenv("POSTGRES_USER"),
            password=os.getenv("POSTGRES_PASSWORD"),
            host=os.getenv("POSTGRES_HOST"),
    ) as connection:
        with connection.cursor() as cursor:
            cursor.execute("UPDATE writer SET requests_count = requests_count + 1;")


@app.route("/")
def index() -> Response:
    requests()

    return Response(f"Hello, cloud lab 2!")


def create_app() -> Flask:
    return app


def run_app() -> None:
    app = create_app()
    app.run(host="0.0.0.0", port=5000)


if __name__ == '__main__':
    run_app()