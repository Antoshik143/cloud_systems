##### University: [ITMO University](https://itmo.ru/ru/)
##### Faculty: [FICT](https://fict.itmo.ru)
##### Course: Cloud computing and services
Year: 2023/2024

Group: K34202

Lab: Lab1*

## Отчёт по лабораторной работе №1* "Работа с Dockerfile"

### Цель работы
Написать образ, запускающий приложение в контейнере, которое должно записывать изменения в базу данных.

### Ход работы

##### 1. Написание приложения для записи в БД
[Приложение](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab1-star/app.py), написанное на python, делает запись в базу данных PosgreSQL при каждом обращении к серверу.

Результат записи:

![Write](./pictures/write_to_db.png)

##### 2. Сохранение содержимого контейнера
Для сохранения содержимого контейнера мы примонтировали директорию контейнера к директории хоста.
Результат:
![Volume](./pictures/volume.png)

##### 3. Создание Docker-образа

Данный [Docker-образ](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab1-star/Dockerfile_app) собран на базе образа python-alpine, устанвливает все необходимые пакеты, копирует файл app.py и запускает.

##### 4.Создание Docker-compose
[Docker-compose файл состоит](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab1-star/docker-compose.yml) из 2 сервисов: база данных - PostgreSQL и само приложение, образ для которого собирается внутри yml файла. Для сборки дополнительно используется файл variables.env, в котором прописаны переменные для создания базы данных.
Используемые образы для запуска приложения и БД.
![Images](./pictures/images.png)

Ниже представлен процесс сборки и запуска контейнеров
![Build_1](./pictures/build1.png)
![Build_2](./pictures/build2.png)
![Build_3](./pictures/build3.png)
![Build_4](./pictures/build4.png)

### Вывод
В ходе выполнения лабораторной работы был написан образ и docker-compose.yml, запускающий приложение в контейнере, которое записывает изменения в базу данных.

