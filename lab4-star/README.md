##### University: [ITMO University](https://itmo.ru/ru/)
##### Faculty: [FICT](https://fict.itmo.ru)
##### Course: Cloud computing and services
Year: 2023/2024

Group: K34202

Lab: Lab4-star

## Отчёт по лабораторной работе №4-star "Создание оповещения при помощи AlertManager"

### Цель работы
Настроить алерт кодом, создав пример с его срабатыванием и оповещением в Телеграм.

### Ход работы

#### Поднимем Prometheus
Создадим namespace monitoring.
```
kubectl create namespace monitoring
```

После чего создадим файлы:
* [clusterRole.yml](./prometheus/clusterRole.yml)
* [config-map.yml](./prometheus/config-map.yml)
* [prometheus-deployment.yml](./prometheus/prometheus-deployment.yml)

Запустим их командой:
```
kubectl create -f clusterRole.yaml
kubectl create -f config-map.yaml
kubectl create  -f prometheus-deployment.yaml 
```

Настроим мапинг портов:
![](./pictures/portprom.png)

#### Настроим Alermanager
Для начала создадим бота телеграм, для этого найдет бота [@BotFather](https://t.me/BotFather) и создадим бота, запомним токен.
![](./pictures/bf.png)

 Тепер напишем  сообщение в чат с нашим ботом и используя [@RawDataBot](https://t.me/RawDataBot) узнаем id чата.

![](./pictures/id.png)

Теперь создадим [AlertManagerConfigmap.yml](./alertmanager/AlertManagerConfigmap.yml). Он содержит конфигурацию пути к шаблону оповещений, электронной почты и другие конфигурации получения оповещений. Укажем телеграм как получателя, так же укажем наш токен и id.
```
kind: ConfigMap
apiVersion: v1
metadata:
  name: alertmanager-config
  namespace: monitoring
data:
  config.yml: |-
    global:
    templates:
    - '/etc/alertmanager/*.tmpl'
    route:
      receiver: telegram
      group_by: ['alertname', 'priority']
      group_wait: 10s
      repeat_interval: 30m
      routes:
        - receiver: telegram
          match:
            severity: 'warning'
          group_wait: 10s
          repeat_interval: 1m
 
    receivers:
    - name: telegram
      telegram_configs:
      - api_url: https://api.telegram.org
        # set up your bot token
        bot_token: <TOKEN_BOT>
        # to get your chat id you can use 
        # https://t.me/RawDataBot and paste chat.id
        chat_id: <CHAT_ID>
        disable_notifications: false
        http_config:
          follow_redirects: true
        send_resolved: true
        parse_mode: 'HTML'
```

Также создадим [AlertTemplateConfigMap.yml](./alertmanager/AlertTemplateConfigMap.yml), [deployment.yml](./alertmanager/deployment.yml) и [service.yml](./alertmanager/service.yml).

Выполним команды:
```
kubectl create -f AlertManagerConfigmap.yml
kubectl create -f AlertTemplateConfigMap.yml
kubectl create -f deployment.yml
kubectl create -f service.yml
```

Получим все наши созданные ресурсы:

![](./pictures/get_all.png)

Также проверим корректность создания в панели управления Kubernetes:

![](./pictures/pods.png)
![](./pictures/deployments.png)
![](./pictures/configm.png)

Настроим мапинг портов:

![](./pictures/portalert.png)

Теперь мы можем получить доступ к диспетчеру оповещений на порту 9090:

![](./pictures/alertm.png)

Спустя какое то время получаем оповещение в чате с ботом в телегам:

![](./pictures/bot.png)

### Вывод
В результате выполнения работы настроен алерт кодом и создан пример с его срабатыванием и оповещением в Телеграм.
