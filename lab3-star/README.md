##### University: [ITMO University](https://itmo.ru/ru/)
##### Faculty: [FICT](https://fict.itmo.ru)
##### Course: Cloud computing and services
Year: 2023/2024

Group: K34202

Lab: Lab3-star

## Отчёт по лабораторной работе №3-star "Создание пайплайна с использованием секретов"

### Цель работы
Создать секрет для GitLab CI, используя Hashicorp Vault.

### Ход работы

#### 1. Настройка Hashicorp Vault

Для начала, используя ряд команд ниже, мы создали директорию для хранилища, скачали и распокавали архив с бинарным файлом vault, выдали ему необходимые права, изменили владельца, и переместили в директорию /usr/local/bin:

```
$ mkdir ~/vault
$ cd ~/vault
$ curl https://releases.hashicorp.com/vault/1.13.2/vault_1.13.2_linux_amd64.zip -o vault_1.13.2_linux_amd64.zip
$ curl https://releases.hashicorp.com/vault/1.13.2/vault_1.13.2_SHA256SUMS -o vault_1.13.2_SHA256SUMS
$ sha256sum --ignore-missing -c vault_1.13.2_SHA256SUMS
$ unzip vault_1.13.2_linux_amd64.zip
$ chmod 755 vault
$ sudo chown root:root vault
$ sudo mv vault /usr/local/bin

```

После чего, используя команду ниже запустили vault хранилище, создав токен root:

```
vault server -dev -dev-root-token-id root
```

![Vault_up](./pictures/vault-server.png)

После запуска сервера мы дополнительно экспортировали переменную, указав адрес и порт, который слушает vault - 8200:

```
export VAULT_ADDR='http://0.0.0.0:8200'
```



После данной подготовки мы перешли в браузер и вбили в строке поиска адрес, по которому попали в UI нашего хранилища:
```
http://127.0.0.1:8200
```

Для авторизации использовали метод Token и содержимым, которое задали при запуске сервера - root. Успешно авторизовавшись, попали на главную страницу Vault хранилища.

![Vault_sign](./pictures/vault_start.png)

#### 2. Настройка Vault

Все взаимодейстиве с Vault мы производили через терминал, но также можно это делать через графический интерфейс. 

Для начала мы создали секрет, который будет хранить наш пароль от Docker Hub

![Create secret](./pictures/create_secret.png)

Чтобы проверить создание секрета можно использовать следующую команду:
```
vault kv get -field=password secret/vault-training/staging/dockerhub
```

Далее мы настроили политику безопасности для данного секрета, выдав ему право только для чтения

![Create policy](./pictures/create_policy.png)

С помощью следующей команды мы можем также проверить создание политики:

```
vault policy read vault-training-staging
```

Затем мы настроили аутентификацию JWT, используя команды на рисунке ниже (JWT - это открытый стандартный метод безопасного взаимодействия между двумя сторонами). 

![](./pictures/auth.png)

**jwks_url="https://gitlab.com/-/jwks" – это URL-адрес HTTPS для нашего экземпляра GitLab**

Финальным шагом настройки является настройка ролей аутентификации. Аутентифицирующие клиенты включают в роль, к которой они хотят получить доступ при подключении, а также множество другой информации.

Создание роли

![Create role](./pictures/create_role.png)

Все наши настройки мы также проверили через UI Vault:

**Secret**
![](./pictures/secret.png)

**Authentication Methods**
![](./pictures/auth_methods.png)

**Policies**
![](./pictures/policy.png)

#### 3. настройка GitLab

Для начала мы на самом GitLab добавили наш проект cloud_systems в группу проектов, к которым разрешен доступ при помощи токена CI_JOB_TOKEN

![CI_JOB_TOKEN](./pictures/ci_job_token.png)

Используя файл из лабораторной работы №3 создали файл [.gitlab-ci.yml](../.gitlab-ci.yml), который срабатывает при пуше в ветку main нашего проекта: 

```
CI_JOB_JWT:
  image: hashicorp/vault:latest
  rules:
    - if: $CI_COMMIT_REF_NAME # Otherwise use the staging role
      variables:
        VAULT_AUTH_ROLE: 'vault-training-staging'

  script:
    # This is your Vault server address
    - export VAULT_ADDR=http://127.0.0.1:8200
    - export VAULT_TOKEN=root

    # Check what role we're using
    - echo $VAULT_AUTH_ROLE

    # Authenticate to the vault server
    - export VAULT_TOKEN="$(vault write -field=token auth/jwt/login role=$VAULT_AUTH_ROLE jwt=$CI_JOB_JWT)"

    # Attempt to get the staging password - only works on `main`
    - export PASSWORD="$(vault kv get -field=password secret/vault-training/staging/dockerhub)"

    - export CI_REGISTRY_IMAGE=anton143/flask-docker:v1.0

    - docker login -u $CI_REGISTRY_USER -p $PASSWORD $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE
```

Gitlab-runner, запущенный локально получает секрет из Vault, содержащий пароль для аутентификации в DockerHub, после чего пушит в Registry Docker-image с названием anton143/flask-docker:v1.0.

#### 4. Выполнение CI

После завершения настройки, нам лишь остается сделать коммит в main ветку репозитория после чего начнется сборка, которая запушить docker image в наш registry.

![CI Job](./pictures/CI-job.png)

Результат добавленного образа на Docker Hub

![Docker Hub](./pictures/dockerhub.png)

### Вывод

В результате выполнения лабораторной работы был создан CI-пайплайн с использованием Vault-хранилища.