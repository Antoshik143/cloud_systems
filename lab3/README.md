##### University: [ITMO University](https://itmo.ru/ru/)
##### Faculty: [FICT](https://fict.itmo.ru)
##### Course: Cloud computing and services
Year: 2023/2024

Group: K34202

Lab: Lab3

## Отчёт по лабораторной работе №3 "Настройка CI/CD"

### Цель работы
Создать push в репозиторий для автоматической сборки Docker Image и сохранения этого Image на DockerHub.

### Ход работы

#### 1. Установка GitLab Runner

Для начала мы установили GitLab Runner к себе на хост в виде демон-службы. Runner в GitLab позволяют автоматизировать рутинные задачи при обновлении файлов в репозитории. Для того чтобы у GitLab был доступ к хосту (серверу), необходимо установить службу gitlab-runner. Runner будет забирать новые исходники с GitLab и собирать образ.

![GitLab-runner 1](./pictures/gitlab-runner1.png)
![GitLab-runner 2](./pictures/gitlab-runner2.png)
![GitLab-runner 3](./pictures/gitlab-runner3.png)

Конфигурационный файл находится по пути /etc/gitlab/config.toml на хосте, на котором запущен runner.
Содержимое config.toml:
![config.toml](./pictures/runner-config.png)

Т. к. мы устанавливали службу GitLab Runner, используя токен, на самом GitLab появился Runner, который показывает связь между аккаунтом и службой.

![GitLab Runners](./pictures/runner.png)

#### 2. Создание Variables

Для получения возможности запушить собранный нами образ в Docker Registry, нам необходимо добавить данные для подключения, такие как логин, пароль и т. д. Но хранить такие данные в файле небезопасно, поэтому мы добавили переменные в GitLab.

![Variables](./pictures/variables.png)

Данные переменные будут использоваться для подключения в DockerHub, а также для тэгирования создаваемого образа.

#### 3. Создание Ci-пайплайна

В корневой директории проекта мы создали файл [gitlab-ci.yml](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/.gitlab-ci.yml), в котором расположили следующий код:
```
image: docker:20.10.16
stages:
  - delivery
services:
  - docker:20.10.16-dind
before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

Delivery:
  stage: delivery
  only:
    - main
  script:
    - docker build --pull -t $CI_REGISTRY_IMAGE ./lab3/.
    - docker images
    - docker push $CI_REGISTRY_IMAGE
    
```
Структура корневой директории после добавления файла
![Main](./pictures/main.png)

Данный файл использует сервис docker:dind, что означает использование docker in docker. Мы логинимся, используя переменные среды к аккаунту на dockerhub, после чего собираем наш образ, добавляя к нему определенный тэг, просматриваем созданные образы и пушим образ в репозиторий на DockerHub.
**Важно:** Автоматическая сборка будет осуществляться только при коммитах в main ветку.

Процесс выполнения Job
![Ci](./pictures/ci.png)

Результат выполнения Job
![Ci-result](./pictures/ci-result.png)

После выполнения Job'ы мы имеем новый Docker-образ в нашем репозитории на DockerHub
![DockerHub](./pictures/dockerhub.png)

## Вывод:
В ходе выполнения лабораторной работы мы создали автоматическую сборку Docker-образа при пуше изменений проекта в главную ветку, а также добавление собранного образа на Docker Registry.