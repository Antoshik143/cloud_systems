##### University: [ITMO University](https://itmo.ru/ru/)
##### Faculty: [FICT](https://fict.itmo.ru)
##### Course: Cloud computing and services
Year: 2023/2024

Group: K34202

Lab: Lab4

## Отчёт по лабораторной работе №4 "Настройка мониторинга сервиса в Kubernetes"

### Цель работы
Настроить мониторинг Kubernetes при помощи Prometheus и Grafana.

### Ход работы

#### 1. Установка Prometheus и Grafana

В данной лабораторной работе нам нужно мониторить minikube, поэтому мы развернем Prometheus и Grafana не как демон-службы, а внутри minikube как сервисы.

- **Prometheus**

Для установки Prometheus мы использовали следующие команды:
```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-np
```

Добавили репозиторий Prometheus, установили Helm-схему и открыли порт для доступа к службе prometheus-server.

- **Grafana**

Для установки Grafana мы использовали следующие команды:

```
helm repo add grafana https://grafana.github.io/helm-charts
helm install grafana grafana/grafana
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-np
```

Добавили репозиторий Grafana, установили и открыли сервис Grafana через NodePort, чтобы получить доступ к пользовательскому интерфейсу.

Проверили работоспособность всех сервисов:

```
kubectl get all
NAME                                                     READY   STATUS    RESTARTS   AGE
pod/grafana-6549b8fd4b-psm6b                             1/1     Running   0          33m
pod/nginx-8bcf48c5b-6d64c                                1/1     Running   0          24h
pod/nginx-deployment-8bcf48c5b-sg5k5                     1/1     Running   0          23h
pod/prometheus-alertmanager-0                            1/1     Running   0          46m
pod/prometheus-kube-state-metrics-85596bfdb6-zxfq9       1/1     Running   0          46m
pod/prometheus-prometheus-node-exporter-zb74p            1/1     Running   0          46m
pod/prometheus-prometheus-pushgateway-79745d4495-hnl8d   1/1     Running   0          46m
pod/prometheus-server-7fdb9c87d-hb6m8                    2/2     Running   0          46m

NAME                                          TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
service/grafana                               ClusterIP   10.98.117.86     <none>        80/TCP         33m
service/grafana-np                            NodePort    10.98.71.15      <none>        80:31594/TCP   33m
service/kubernetes                            ClusterIP   10.96.0.1        <none>        443/TCP        24h
service/nginx-deployment                      NodePort    10.100.51.103    <none>        80:31213/TCP   23h
service/prometheus-alertmanager               ClusterIP   10.101.72.204    <none>        9093/TCP       46m
service/prometheus-alertmanager-headless      ClusterIP   None             <none>        9093/TCP       46m
service/prometheus-kube-state-metrics         ClusterIP   10.110.108.150   <none>        8080/TCP       46m
service/prometheus-prometheus-node-exporter   ClusterIP   10.110.96.196    <none>        9100/TCP       46m
service/prometheus-prometheus-pushgateway     ClusterIP   10.98.98.163     <none>        9091/TCP       46m
service/prometheus-server                     ClusterIP   10.105.166.116   <none>        80/TCP         46m
service/prometheus-server-np                  NodePort    10.104.31.193    <none>        80:31823/TCP   46m

NAME                                                 DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
daemonset.apps/prometheus-prometheus-node-exporter   1         1         1       1            1           kubernetes.io/os=linux   46m

NAME                                                READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/grafana                             1/1     1            1           33m
deployment.apps/nginx                               1/1     1            1           24h
deployment.apps/nginx-deployment                    1/1     1            1           23h
deployment.apps/prometheus-kube-state-metrics       1/1     1            1           46m
deployment.apps/prometheus-prometheus-pushgateway   1/1     1            1           46m
deployment.apps/prometheus-server                   1/1     1            1           46m

NAME                                                           DESIRED   CURRENT   READY   AGE
replicaset.apps/grafana-6549b8fd4b                             1         1         1       33m
replicaset.apps/nginx-8bcf48c5b                                1         1         1       24h
replicaset.apps/nginx-deployment-8bcf48c5b                     1         1         1       23h
replicaset.apps/prometheus-kube-state-metrics-85596bfdb6       1         1         1       46m
replicaset.apps/prometheus-prometheus-pushgateway-79745d4495   1         1         1       46m
replicaset.apps/prometheus-server-7fdb9c87d                    1         1         1       46m

NAME                                       READY   AGE
statefulset.apps/prometheus-alertmanager   1/1     46m
```

#### 2. Настройка мониторинга

Мы получили URL-адрес Prometheus:

```
minikube service prometheus-server-np --url
http://192.168.49.2:31823
```

Перейдя по адресу http://192.168.49.2:31823 мы попали на страницу веб-интерфейса Prometheus.

![Prometheus](./pictures/Prometheus.png)

Далее мы получили URL-адрес Grafana:

```
minikube service grafana-np --url
http://192.168.49.2:31594
```

Перейдя по адресу http://192.168.49.2:31594 мы попали на страницу веб-интерфейса Grafana.


После того, как мы попали на страницу Grafana, нам требуется авторизация с логином admin и паролем, который можно получить при помощи команды:
```
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

Чтобы получать графики с метриками, нужно для начала добавить источник метрик. В нашем случае это узел Prometheus.

![Source](./pictures/Data-source.png)

И чтобы создать dashboard, мы перешли на вкладку Dashboards, выбрали создать новый dashboard и далее import. В данном поле можно выбрать готовый шаблон из тех, что представлены на [официальном сайте Grafana Labs](https://grafana.com/grafana/dashboards/). Мы выбради готовый шаблон Node exporter, который собирает различные физические характеристики.

![Dashboard](./pictures/dashboard.png)

### Вывод:
В ходе лабораторной работы мы установили внутрь кластера minikube Prometheus и Grafana и настроили мониторинг кластера и отображение его метрик в dashboard Grafana.
