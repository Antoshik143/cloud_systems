# Cloud_systems


В данном репозитории хранятся выполененные лабораторные работы по дисциплине: "Облачные системы и услуги" 4 курс.

Команда репозитория:
- Давыдов Антон
- Коноваленко Максим
- Тихонов Степан
- Сбитнев Александр

[Отчет по 1 лабораторной работе](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab1/README.md)

[Отчет по 1* лабораторной работе](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab1-star/README.md)

[Отчет по 2 лабораторной работе](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab2/README.md)

[Отчет по 2* лабораторной работе](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab2-star/README.md)

[Отчет по 3 лабораторной работе](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab3/README.md)

[Отчет по 3* лабораторной работе](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab3-star/README.md)

[Отчет по 4 лабораторной работе](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab4/README.md)

[Отчет по 4* лабораторной работе](https://gitlab.com/Antoshik143/cloud_systems/-/blob/main/lab4-star/README.md)