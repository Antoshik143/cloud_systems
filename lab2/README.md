##### University: [ITMO University](https://itmo.ru/ru/)
##### Faculty: [FICT](https://fict.itmo.ru)
##### Course: Cloud computing and services
Year: 2023/2024

Group: K34202

Lab: Lab2

## Отчёт по лабораторной работе №2 "Оркестрация в Kubernetes"

### Цель работы
Поднять локально Kubernetes кластер и развернуть в нем сервис, используя 2-3 ресурса Kubernetes.

### Ход работы

#### 1. Установка minikube
Для локального пользования Kubernetes используется minikube - инстурмент для создания одного кластера.

Для запуска minikube мы использовали следующую команду:
```
minikube start --deriver=docker
```
Мы запустили кластер и в качестве драйвера для простоты использовали docker.

![Minikube_start](./pictures/minikube_start.png)

Также установили kubectl - утилита для взаимодействия с кластером, и проверили её работоспособность.

![Kubectl-test](./pictures/kubectl-test.png)

Также узнали IP-адрес нашего кластера: 192.168.49.2

![minikube_ip](./pictures/minikube-ip.png)

#### 2. Написание конфигураций

В кластере minikube мы развернем сервис - контейнер Nginx. Для этого мы используем 2 сущности Kubernetes: deployment и service.

- Deployment - высокоуровненвая модель,  благодаря которой мы описываем желаемое состояние в развертывании.
- Service - это метод предоставления доступа к сетевому приложению, которое работает как один или несколько подов в нашем кластере.

Для написания конфигураций мы использовали yml формат

[Deployment](app-deployment.yml)
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      components: lab2
  template:
    metadata:
      labels:
        components: lab2
    spec:
      containers:
        - name: nginx-server
          image: nginx
          imagePullPolicy: Always
          ports:
            - containerPort: 80
          resources:
            limits:
              memory: "128Mi"
              cpu: "200m"
```

Эта конфигурация прездназначена для создания экземпляра приложения Nginx. 
Ключевые аспекты:

- replicas: 1 - указывает, что должен быть только один экземпляр приложения.
- selector и matchLabels - определяют, какие Pods будут управляться этим Deployment на основе меток.
- template содержит шаблон для создания Pod с метками components: lab2.
- containers - определяет контейнер в Pod, использующий образ Nginx с политикой
- ports - открывает порт 80 в контейнере для взаимодействия с приложением.
- resources - устанавливает ограничения ресурсов для контейнера (ограничение использования памяти до 128Mi и процессора до 200m).


[Service](service.yml)

```
apiVersion: v1
kind: Service
metadata:
  name: service-nginx
spec:
  type: NodePort
  ports:
    - port: 3000
      targetPort: 80
      nodePort: 31200
  selector:
    components: lab2
```

Этот конфигурационный файл описывает Kubernetes Service с названием service-nginx. 
Ключевые аспекты:

- type: NodePort - определяет тип службы Kubernetes как NodePort, позволяющий доступ к сервису через порт на узле кластера.
- ports - настраивает порт службы на 3000, который направляет трафик на targetPort 80 внутри подов.
- nodePort: 31200 - задает конкретный порт на узлах кластера, через который можно получить доступ к службе.
- selector - указывает, что этот сервис будет направлять трафик к подам с меткой components: lab2.

#### 3. Применение конфигураций

Для применения конфигураций мы использовали следующую команду:
```
kubectl apply -f app-deployment.yml
```
Результат выполнения:

![kubectl_all](./pictures/kubectl_all.png)

**Подробное описание конфигурации представлено в [файле](kube_deploy_config.txt)**

После чего нам нужно было открыть доступ к контейнеру приложения nginx
```
kubectl expose deployment/nginx-deployment --type="NodePort" --port 80
```
Мы проверили порт и вбили в строке браузера IP-адрес кластера и соответствующий порт - 31213.

![nginx-port](./pictures/nginx-port.png)
![nginx](./pictures/nginx.png)

### Вывод:
В ходе выполнения лабораторной работы был изучен монокластер minikube и написаны файл deployment'а и файл service'а для разворачивания nginx внутри кластера.
